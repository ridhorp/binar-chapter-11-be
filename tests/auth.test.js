import request from "supertest";
import { app } from "../index";

// Positive
describe("Test Sign In User", () => {
  test("should response token and the data of user", async () => {
    const response = await request(app).post("/api/auth/signIn").send({
      username: "luckman558",
      password: "luckman558",
    });
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty("token");
    expect(response.body).toHaveProperty("user");
  });
});

describe("Test get all user", () => {
  test("should response the data of all users", async () => {
    const response = await request(app).get("/api/user");
    expect(response.statusCode).toBe(200);
  });
});

// Negative

describe("Test Sign Up User that already exists", () => {
  test("should response status code of 400 and the error message", async () => {
    const response = await request(app)
      .post("/api/auth/signup")
      .send({
        name: "lukman123",
        username: "lukman123",
        email: "lukman123@gmail.com",
        password: "123456",
        img: "test",
        score: [
          { gameName: "RSP", point: 0 },
          { gameName: "Dummy", point: 0 },
        ],
      });
    expect(response.body).toHaveProperty("error");
  });
});

describe("Test Sign In User with wrong credentials", () => {
  test("should response error message", async () => {
    const response = await request(app).post("/api/auth/signIn").send({
      username: "luckman5581",
      password: "luckman558",
    });
    expect(response.statusCode).toBe(400);
    expect(response.body).toHaveProperty("error");
  });
});

describe("Test Updating user with the wrong id", () => {
  test("should response error message", async () => {
    const response = await request(app).post("/api/user/abcdef").send({
      username: "luckman5581",
      password: "luckman558",
    });
    expect(response.statusCode).toBe(404);
  });
});
